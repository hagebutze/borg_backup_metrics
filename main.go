package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	//        "golang.org/x/oauth2/google"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

/*
*
Info exampple

	{
	    "cache": {
	        "path": "/root/.cache/borg/2e2a56e9a9c0c10ca815f924155bb1e70f496bfe3d6214d9b55a43a7aafb34fc",
	        "stats": {
	            "total_chunks": 76,
	            "total_csize": 17256252,
	            "total_size": 19666346,
	            "total_unique_chunks": 76,
	            "unique_csize": 17256252,
	            "unique_size": 19666346
	        }
	    },
	    "encryption": {
	        "mode": "repokey"
	    },
	    "repository": {
	        "id": "2e2a56e9a9c0c10ca815f924155bb1e70f496bfe3d6214d9b55a43a7aafb34fc",
	        "last_modified": "2020-12-03T20:16:04.000000",
	        "location": "/backup/backup"
	    },
	    "security_dir": "/root/.config/borg/security/2e2a56e9a9c0c10ca815f924155bb1e70f496bfe3d6214d9b55a43a7aafb34fc"
	}
*/
type BorgRepoInfoCacheStats struct {
	TotalChunks       int `json:"total_chunks"`
	TotalCSize        int `json:"total_csize"`
	TotalUniqueChunks int `json:"total_unique_chunks"`
	UniqueCSize       int `json:"unique_csize"`
	UniqueSize        int `json:"unique_size"`
}

type BorgRepoInfoCache struct {
	Path  string                 `json:"path"`
	Stats BorgRepoInfoCacheStats `json:"stats"'`
}

type BorgRepoInfoEncryption struct {
	Mode string `json:"mode"`
}

type BorgRepoInfoRepository struct {
	Id           string `json:"id"`
	LastModified string `json:"last_modified"`
	Location     string `json:"locations"`
}

type BorgRepoInfo struct {
	Cache       BorgRepoInfoCache      `json:"cache"`
	Encryption  BorgRepoInfoEncryption `json:"encryption"`
	Repository  BorgRepoInfoRepository `json:"repository"`
	SecurityDir string                 `json:"security_dir"`
}

type ResticRepoSnapshotSummary struct {
	BackupStart         string `json:"backup_start"`
	BackupEnd           string `json:"backup_end"`
	FilesNew            int    `json:"files_new"`
	FilesChanged        int    `json:"files_changed"`
	FilesUnmodified     int    `json:"files_unmodified"`
	DirsNew             int    `json:"dirs_new"`
	DirsChanged         int    `json:"dirs_changed"`
	DataBlobs           int    `json:"data_blobs"`
	TreeBlobs           int    `json:"tree_blobs"`
	DataAdded           int    `json:"data_added"`
	DataAddedPacked     int    `json:"data_added_packed"`
	TotalFilesProcessed int    `json:"total_files_processed"`
	TotalBytesProcessed int    `json:"total_bytes_processed"`
}

type ResticRepoSnapshot struct {
	Id             string                    `json:"id"`
	ShortId        string                    `json:"short_id"`
	Parent         string                    `json:"parent"`
	Tree           string                    `json:"tree"`
	Paths          []string                  `json:"paths"`
	Hostname       string                    `json:"hostname"`
	Username       string                    `json:"username"`
	Uid            int                       `json:"uid"`
	Gui            int                       `json:"gid"`
	ProgramVersion string                    `json:"program_version"`
	Summary        ResticRepoSnapshotSummary `json:"summary"`
}

type ResticRepoInfo = struct {
	Snapshots []ResticRepoSnapshot
}

var (
	lastRepoInfoUpdate time.Time                 = time.Unix(0, 0)
	borgRepoInfo       map[string]BorgRepoInfo   = map[string]BorgRepoInfo{}
	resticRepoInfo     map[string]ResticRepoInfo = map[string]ResticRepoInfo{}
	registry                                     = prometheus.NewRegistry()
)

func UpdateRepoInfos() error {
	if lastRepoInfoUpdate.Add(time.Minute * 5).Before(time.Now()) {
		// Borg
		for name := range borgRepoInfo {
			fmt.Printf("Gathering info for %s using borg\n", name)
			command := exec.Command("/usr/bin/borg", "info", name, "--json")
			output, err := command.Output()
			if err != nil {
				fmt.Printf("Error on running command %v\n", err)
				return err
			}
			var newInfo BorgRepoInfo = BorgRepoInfo{}
			err = json.Unmarshal(output, &newInfo)
			if err != nil {
				fmt.Printf("Error on running unmarshal %v\n", err)
				return err
			}
			borgRepoInfo[name] = newInfo
		}

		// Restic
		for name := range resticRepoInfo {
			fmt.Printf("Gathering info for %s using restic\n", name)
			command := exec.Command("/usr/bin/restic", "-r", name, "snapshots", "--json")
			output, err := command.Output()
			if err != nil {
				fmt.Printf("Error on running command %v\n", err)
				return err
			}
			var newResticSnapshots []ResticRepoSnapshot = []ResticRepoSnapshot{}
			err = json.Unmarshal(output, &newResticSnapshots)
			if err != nil {
				fmt.Printf("Error on running unmarshal %v\n", err)
				return err
			}
			resticRepoInfo[name] = ResticRepoInfo{Snapshots: newResticSnapshots}
		}
		lastRepoInfoUpdate = time.Now()
	}

	return nil
}

func init() {
	// Get backup directories
	borgBackupRepos := strings.Split(os.Getenv("BORG_BACKUP_REPOS"), " ")
	// Create repo infos
	for _, backupRepo := range borgBackupRepos {
		fmt.Printf("Initilizing for %s\n", backupRepo)
		borgRepoInfo[backupRepo] = BorgRepoInfo{}
		localBackDirectorieVar := backupRepo
		registry.MustRegister(prometheus.NewGaugeFunc(
			// Register age
			prometheus.GaugeOpts{
				Name: fmt.Sprintf("age_of%s", strings.Replace(localBackDirectorieVar, "/", "_", -1)),
				Help: fmt.Sprintf("Reports the backup age in seconds for %s", localBackDirectorieVar),
			}, func() float64 {
				UpdateRepoInfos()
				modTime, err := time.ParseInLocation("2006-01-02T15:04:05.000000", borgRepoInfo[localBackDirectorieVar].Repository.LastModified, time.Now().Location())
				if err != nil {
					fmt.Printf("Error %v\n", err)
					return 0.0
				}
				return float64(time.Now().Unix() - modTime.Unix())
			},
		))
		registry.MustRegister(prometheus.NewGaugeFunc(
			// Register size
			prometheus.GaugeOpts{
				Name: fmt.Sprintf("size_of%s", strings.Replace(localBackDirectorieVar, "/", "_", -1)),
				Help: fmt.Sprintf("Reports the backup age in seconds for %s", localBackDirectorieVar),
			}, func() float64 {
				UpdateRepoInfos()
				return float64(borgRepoInfo[localBackDirectorieVar].Cache.Stats.UniqueSize)
			},
		))
	}

	// Get backup directories for retic
	resticBackupRepos := strings.Split(os.Getenv("RESTIC_BACKUP_REPOS"), " ")
	// Create repo infos
	for _, backupRepo := range resticBackupRepos {
		fmt.Printf("Initilizing for %s\n", backupRepo)
		resticRepoInfo[backupRepo] = ResticRepoInfo{}
		localBackDirectorieVar := backupRepo
		registry.MustRegister(prometheus.NewGaugeFunc(
			// Register age
			prometheus.GaugeOpts{
				Name: fmt.Sprintf("age_of%s", strings.Replace(
					strings.Replace(localBackDirectorieVar, "/", "_", -1),
					":", "_", -1,
				),
				),
				Help: fmt.Sprintf("Reports the backup age in seconds for %s", localBackDirectorieVar),
			}, func() float64 {
				UpdateRepoInfos()
				modTime, err := time.ParseInLocation("2006-01-02T15:04:05.000000", resticRepoInfo[localBackDirectorieVar].Snapshots[0].Summary.BackupStart, time.Now().Location())
				if err != nil {
					fmt.Printf("Error %v\n", err)
					return 0.0
				}
				return float64(time.Now().Unix() - modTime.Unix())
			},
		))
		registry.MustRegister(prometheus.NewGaugeFunc(
			// Register size
			prometheus.GaugeOpts{
				Name: fmt.Sprintf("size_of%s", strings.Replace(localBackDirectorieVar, "/", "_", -1)),
				Help: fmt.Sprintf("Reports the backup age in seconds for %s", localBackDirectorieVar),
			}, func() float64 {
				UpdateRepoInfos()
				return float64(resticRepoInfo[localBackDirectorieVar].Snapshots[0].Summary.TotalBytesProcessed)
			},
		))
	}
}

func main() {
	fmt.Println("Starting")
	http.Handle("/metrics", promhttp.HandlerFor(
		registry,
		promhttp.HandlerOpts{},
	))
	http.ListenAndServe(":2112", nil)
}
